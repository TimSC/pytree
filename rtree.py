#Based on http://www.bowdoin.edu/~ltoma/teaching/cs340/spring08/Papers/Rtree-chap1.pdf
#Original paper https://web.archive.org/web/20150906054832/http://pages.cs.wisc.edu:80/~cs764-1/rtree.pdf
from __future__ import print_function

class Mbr(object):
	'''Minimum bounding rectangle'''

	def __init__(self, shapeIn=None):

		if isinstance(shapeIn, Mbr):
			self.shape = shapeIn.shape

		elif shapeIn is None:
			self.shape = None

		elif len(shapeIn) == 4:
			self.shape = list(shapeIn) #left,bottom,right,top
			if self.shape[2] < self.shape[0] or self.shape[3] < self.shape[1]:
				raise ValueError("Invalid MBR shape")

		elif len(shapeIn) == 2:
			self.shape = [shapeIn[0], shapeIn[1], shapeIn[0], shapeIn[1]]

		else:
			raise ValueError("Shape must have 2 or 4 components")

	def area(self):
		if self.shape is None: return 0.0
		area = (self.shape[2] - self.shape[0]) * (self.shape[3] - self.shape[1])
		assert area >= 0.0
		return area

	def get_enlarged(self, pos):

		if isinstance(pos, Mbr):

			if self.shape is not None:

				#Treat input as mbr shape
				newShape = self.shape[:]
				if pos.shape[0] < newShape[0]:
					newShape[0] = pos.shape[0]
				if pos.shape[2] > newShape[2]:
					newShape[2] = pos.shape[2]
				if pos.shape[1] < newShape[1]:
					newShape[1] = pos.shape[1]
				if pos.shape[3] > newShape[3]:
					newShape[3] = pos.shape[3]

			else:
				newShape = pos.shape[:]

		elif len(pos) == 2:

			if self.shape is not None:
				#Treat input as point
				newShape = self.shape[:]
				if pos[0] < newShape[0]:
					newShape[0] = pos[0]
				elif pos[0] > newShape[2]:
					newShape[2] = pos[0]
				if pos[1] < newShape[1]:
					newShape[1] = pos[1]
				elif pos[1] > newShape[3]:
					newShape[3] = pos[1]

			else:
				newShape = [pos[0], pos[1], pos[0], pos[1]]

		else:
			raise ValueError("Invalid number of components for MBR")

		if newShape[2] < newShape[0] or newShape[3] < newShape[1]:
			raise ValueError("Invalid MBR shape")

		return Mbr(newShape)

	def enlarge(self, pos):

		newMbr = self.get_enlarged(pos)
		self.shape = newMbr.shape

		if self.shape[2] < self.shape[0] or self.shape[3] < self.shape[1]:
			raise ValueError("Invalid MBR shape")
			
	def area_increase_on_insert(self, pos):

		enlargedMbr = self.get_enlarged(pos)
		areaIncrease = enlargedMbr.area() - self.area()
		assert areaIncrease >= 0.0
		return areaIncrease

	def contains(self, pos):

		if isinstance(pos, Mbr):
			if pos.shape[0] < self.shape[0] or pos.shape[2] > self.shape[2]: return False
			if pos.shape[1] < self.shape[1] or pos.shape[3] > self.shape[3]: return False

		elif len(pos) == 2:
			#Treat as point
			if pos[0] < self.shape[0] or pos[0] > self.shape[2]: return False
			if pos[1] < self.shape[1] or pos[1] > self.shape[3]: return False

		else:
			raise ValueError("query shape has wrong number of components")

		return True

	def intersects(self, pos):

		if isinstance(pos, Mbr):
			#Treat as rectangle
			if pos.shape[2] < self.shape[0] or pos.shape[0] > self.shape[2]: return False
			if pos.shape[3] < self.shape[1] or pos.shape[1] > self.shape[3]: return False

		else:
			raise ValueError("query shape has wrong number of components")

		return True

	def __str__(self):
		return "Mbr {}".format(self.shape)

def CombineMbrs(mbrs):
	if mbrs is None or len(mbrs)==0:
		return None
	combined = Mbr(mbrs[0]) #Create a copy
	for mbr in mbrs[1:]:
		combined.enlarge(mbr)
	return combined

class RTreeEntry(object):
	def __init__(self, pos, data):	
		self.pos = pos
		self.data = data

	def get_shape(self):
		return Mbr([self.pos[0], self.pos[1], self.pos[0], self.pos[1]])

class RTreeNode(object):
	def __init__(self, mbr=None):
		if mbr is not None:
			self.mbr = mbr #left,bottom,right,top
		else:
			self.mbr = Mbr()
		self.entries = []

	def quadratic_pick_seeds(self):

		if len(self.entries) < 2: return None

		bestArea = None
		bestPair = None
		for i, entry1 in enumerate(self.entries):
			for j, entry2 in enumerate(self.entries[i+1:]):

				pos1 = entry1.get_shape()
				pos2 = entry2.get_shape()
				combined = Mbr(pos1) #Create a copy
				combined.enlarge(pos2)
				area = combined.area() - pos1.area() - pos2.area()

				if bestArea is None or area > bestArea:
					bestArea = area
					bestPair = (i, j)

		if bestPair is None: 
			return None
		return self.entries[bestPair[0]], self.entries[bestPair[1]]

	def insert(self, entryOrCh):

		if isinstance(entryOrCh, RTreeNode) or isinstance(entryOrCh, RTreeEntry):
			self.entries.append(entryOrCh)
			self.mbr.enlarge(entryOrCh.get_shape())
		
		else:
			raise ValueError("Inserting unknown type")

	def __len__(self):

		total = 0
		for ch in self.entries:
			if isinstance(ch, RTreeNode):
				total += len(ch)
			else:
				total += 1
		return total

	def check_integity(self):

		entryType = None
		for entry in self.entries:
			if not self.mbr.contains(entry.get_shape()):
				raise RuntimeError("Entry outside MBR")
			if entryType is None:
				entryType = entry.__class__
			else:
				if not isinstance(entry, entryType):
					raise RuntimeError("Mismatched types in entry list")
		
	def get_shape(self):
		return self.mbr

	def is_leaf(self):
		if len(self.entries) == 0: return True #Probably the root node
		return isinstance(self.entries[0], RTreeEntry)

	def refresh_mbr(self):
		mbrs = [entry.get_shape() for entry in self.entries]
		mbr = CombineMbrs(mbrs)
		self.mbr = mbr

	def get_tree_depth(self):
		d = 1
		cursor = self
		while not cursor.is_leaf() and len(cursor.entries) > 0:
			cursor = cursor.entries[0]
			d += 1
		return d

class RTree(object):
	
	def __init__(self, maxEntries = 50, minEntries = 25, debug = False, method = 'quadratic'):
		self.rn = RTreeNode()
		self.maxEntries = maxEntries
		self.minEntries = minEntries
		self.debug = debug
		self.method = method
		if self.minEntries > self.maxEntries//2:
			raise ValueError("minEntries too high")

	def _plan_split(self, cursor):

		#Find seeds to start growing regions O(n^2)
		entry1, entry2 = cursor.quadratic_pick_seeds()
		#TODO add linear O(n) seed selection

		n1 = RTreeNode(entry1.get_shape())
		n2 = RTreeNode(entry2.get_shape())

		#Sort by x position (quicksort average performance O(n log n))
		sortableEntries = cursor.entries[:]
		if self.method == 'nlogn':
			sortableEntries.sort(key=lambda entry: entry.get_shape().shape[0])
		numToSort = len(sortableEntries)

		#Add each entry into the most appropriate node, but stop if we need to
		#meet a minEntries requirement.
		remainingEntries = sortableEntries[:]
		while len(remainingEntries) > 0 and \
			self.minEntries < len(n1.entries)+len(remainingEntries) and \
			self.minEntries < len(n2.entries)+len(remainingEntries):

			if self.method == 'quadratic':
				#This is the quadratic search (O(n^2) complexity)
				entry, remainingEntries = self._quadratic_pick_next(n1, n2, remainingEntries)

			else:
				#This is the linear search (as in linear O(n) in the time/entry relationship)
				entry = remainingEntries.pop(0)

			area1 = n1.mbr.area_increase_on_insert(entry.get_shape())
			area2 = n2.mbr.area_increase_on_insert(entry.get_shape())
			if area1 < area2:
				n1.insert(entry)
			elif area2 < area1:
				n2.insert(entry)
			elif n1.mbr.area() < n2.mbr.area():
				n1.insert(entry)
			elif n2.mbr.area() < n1.mbr.area():
				n2.insert(entry)
			elif len(n1.entries) < len(n2.entries):
				n1.insert(entry)
			else:
				n2.insert(entry)

		if self.minEntries==len(n1.entries)+len(remainingEntries):

			#Insert remaining entries into n1 to meet minimum number
			for entry2 in remainingEntries:
				n1.insert(entry2)

		elif self.minEntries==len(n2.entries)+len(remainingEntries):

			#Insert remaining entries into n2 to meet minimum number
			for entry2 in remainingEntries:
				n2.insert(entry2)

		elif len(remainingEntries) > 0:
			raise RuntimeError("Could not meet minimum number of entries requirement")

		if self.debug:
			assert len(n1.entries) >= self.minEntries and len(n1.entries) <= self.maxEntries
			assert len(n2.entries) >= self.minEntries and len(n2.entries) <= self.maxEntries
			n1.check_integity()
			n2.check_integity()

		return n1, n2

	def  _quadratic_pick_next(self, n1, n2, remainingEntries):
		
		bestDiff = None
		bestI = None

		for i, entry in enumerate(remainingEntries):

			n1s = n1.get_shape()
			n2s = n2.get_shape()
			es = entry.get_shape()

			d1 = n1s.area_increase_on_insert(es)
			d2 = n2s.area_increase_on_insert(es)
			
			diff = abs(d1 - d2)

			if bestDiff is None or diff > bestDiff:
				bestDiff = diff
				bestI = i

		rEnt = remainingEntries[:]
		entry = rEnt.pop(bestI)

		return entry, rEnt

	def _find_leaf_for_insert(self, shape, insertDepth=None):

		cursor = self.rn
		pathToCursor = [cursor,]
		depth = 1
		while not cursor.is_leaf() and \
			(insertDepth is None or depth < insertDepth):

			bestIncrease = None
			bestArea = None
			bestCh = None
			for i, ch in enumerate(cursor.entries):
				mbr = ch.get_shape()
				areaIncrease = mbr.area_increase_on_insert(shape)
				mbrArea = mbr.area()

				if bestIncrease is None or areaIncrease < bestIncrease or \
					(areaIncrease == bestIncrease and mbrArea < bestArea):

					bestIncrease = areaIncrease
					bestArea = mbrArea
					bestCh = ch

			assert cursor is not bestCh
			cursor = bestCh
			pathToCursor.append(bestCh)
			depth += 1

		pathToCursor.pop(-1)
		return cursor, pathToCursor

	def insert(self, pos, data=None, insertDepth=None):

		if isinstance(pos, RTreeEntry):
			entry = pos
		elif isinstance(pos, RTreeNode):
			entry = pos
		else:
			entry = RTreeEntry(pos, data)

		#Find appropriate leaf node that will result in minimum MBR enlargement
		#This is named ChooseLeaf in the original paper
		cursor, pathToCursor = self._find_leaf_for_insert(entry.get_shape(), 
			insertDepth)

		#Insert data into selected leaf node			
		cursor.insert(entry)

		splitNeeded = len(cursor.entries) > self.maxEntries
		parent = None
		if len(pathToCursor) > 0:
			parent = pathToCursor[-1]
		assert id(parent) != id(cursor)
		
		if splitNeeded:

			n1, n2, parent = self._do_split(cursor, parent)
			del cursor
			parent.mbr.enlarge(n1.get_shape())
			parent.mbr.enlarge(n2.get_shape())

		elif parent is not None:
			parent.mbr.enlarge(cursor.get_shape())

		#Update ancestor MBRs
		walkUpToRoot = pathToCursor[::-1]
		prevAncestor = None
		for i, ancestor in enumerate(walkUpToRoot):
			if i > 0:
				ancestor.mbr.enlarge(prevAncestor.mbr)
			prevAncestor = ancestor

		if splitNeeded:
			#Check if internal nodes need a split
			for i, node in enumerate(walkUpToRoot):

				if len(node.entries) > self.maxEntries:
				
					if i+1 < len(walkUpToRoot):
						parent = walkUpToRoot[i+1]
					else:
						parent = None #Root has no parent
					self._do_split(node, parent)

		return entry

	def _do_split(self, cursor, parent):

		n1, n2 = self._plan_split(cursor)

		if id(cursor) == id(self.rn):
			#Split root node
			self.rn.entries=[n1, n2]
			self.rn.mbr=CombineMbrs([n1.mbr, n2.mbr])
			parent = self.rn

		else:
			#Split leaf node
			parent.entries.remove(cursor)
			parent.entries.extend([n1, n2])

		return n1, n2, parent

	def _check_integrity_node(self, n):

		assert len(n.entries) <= self.maxEntries
		
		if id(n) != id(self.rn):
			assert len(n.entries) >= self.minEntries
		
		n.check_integity()
		
		for ch in n.entries:
			if isinstance(ch, RTreeNode):
				self._check_integrity_node(ch)

	def check_integrity(self):

		self._check_integrity_node(self.rn)

	def get_tree_depth(self):

		return self.rn.get_tree_depth()

	def search(self, rect, node=None, searchMetrics=None, bruteForce=False):

		if isinstance(rect, Mbr):
			pass
		elif len(rect) == 4:
			rect = Mbr(rect)	
		else:
			raise ValueError("Query rectangle must have 4 components")
		if node is None:
			node = self.rn
		
		if not bruteForce and not node.mbr.intersects(rect):

			if isinstance(searchMetrics, dict):
				if 'leaf_skipped' not in searchMetrics:
					searchMetrics['leaf_skipped'] = []
				searchMetrics['leaf_skipped'].append(id(node))

			return []

		out = []
		if len(node.entries) > 0:
			for entry in node.entries:
				if isinstance(entry, RTreeEntry) and rect.contains(entry.pos):
					out.append(entry)

			if isinstance(searchMetrics, dict):
				if 'leaf_searched' not in searchMetrics:
					searchMetrics['leaf_searched'] = []
				searchMetrics['leaf_searched'].append(id(node))

		for ch in node.entries:
			if isinstance(ch, RTreeNode):
				out.extend(self.search(rect, ch, searchMetrics, bruteForce))

		return out

	def __len__(self, node=None):

		return len(self.rn)

	def _find_leaf_for_delete(self, entryToFind, node=None, pathToNode=None):

		if node is None:
			node = self.rn
		if pathToNode is None:
			pathToNode = []

		if node.is_leaf():
			for entry in node.entries:
				if id(entry) == id(entryToFind):
					return (pathToNode, node)

		else:
			for entry in node.entries:
				if not entry.mbr.contains(entryToFind.pos):
					continue

				pathToNodeCopy = pathToNode[:]
				pathToNodeCopy.append(node)
				ret = self._find_leaf_for_delete(entryToFind, entry, pathToNodeCopy)
				if ret is not None: 
					return ret

		return None

	def delete(self, entry):

		#Find node containing entry
		pathToLeaf, leaf = self._find_leaf_for_delete(entry)

		#Remove entry from leaf node
		leaf.entries.remove(entry)

		#Condense tree
		nodesToReinsert = []
		cursor = leaf
		for parent in pathToLeaf[::-1]:

			if len(cursor.entries) < self.minEntries:
				#Node is underfull.
				
				parent.entries.remove(cursor)
				nodesToReinsert.extend(cursor.entries)

			else:
				cursor.refresh_mbr()

			#Prepare for next iteration
			cursor = parent

		if id(leaf) != id(self.rn):
			self.rn.refresh_mbr()

		#Reinsert nodes
		for node in nodesToReinsert:

			if isinstance(node, RTreeEntry):
				self.insert(node)

			elif isinstance(node, RTreeNode):
				subtree_depth = node.get_tree_depth()
				self.insert(node, insertDepth = self.get_tree_depth() - subtree_depth)

			else:
				raise RuntimeError("Unknown type cannot be reinserted")
		
		#Shorten tree if needed
		if not self.rn.is_leaf() and len(self.rn.entries) == 1:
			self.rn = self.rn.entries[0]
		
