from __future__ import print_function
from matplotlib import pyplot as plt
import matplotlib.patches as patches
import random
import rtree
import zipfile
import csv
import io

def PrintTree(node, depth=0):
	assert isinstance(node, rtree.RTreeNode)

	for d in range(depth):
		print ("\t", end='')
	print ("node", id(node), node.mbr, len(node.entries))

	for ch in node.entries:
		if isinstance(ch, rtree.RTreeNode):
			PrintTree(ch, depth+1)

def PlotTree(node, fig, ax, depth=0):
	assert isinstance(node, rtree.RTreeNode)

	s = node.mbr.shape
	col = 'r'
	if len(node.entries) > 0:
		col = 'b'

	rect = patches.Rectangle((s[0],s[1]),s[2]-s[0],s[3]-s[1],linewidth=1,edgecolor=col,facecolor='none')
	ax.add_patch(rect)

	if node.is_leaf():
		pts = [entry.pos for entry in node.entries]
		ptsX = [pt[0] for pt in pts]
		ptsY = [pt[1] for pt in pts]
		if len(ptsX) > 0:
			ax.plot(ptsX, ptsY, 'g.')

	for ch in node.entries:
		if isinstance(ch, rtree.RTreeNode):
			PlotTree(ch, fig, ax, depth+1)

if __name__=="__main__":

	#Test data from https://www.ordnancesurvey.co.uk/business-government/products/code-point-open
	fi = zipfile.ZipFile("codepo_gb.zip")

	inf = fi.getinfo("Data/CSV/po.csv")

	data = fi.open(inf)
	dataDecOriginal = csv.reader(io.TextIOWrapper(data))

	dataDecOriginal = list(dataDecOriginal)
	dataDec = dataDecOriginal[:1000]
	random.shuffle(dataDec)

	searchBox = rtree.Mbr([464300, 100400, 464800, 100800])

	rt = rtree.RTree(10, 5)
	rt.debug = True
	bruteForceSearch = []	
	entries = []

	for i, li in enumerate(dataDec):
		if i % 1000 == 0:
			print (i, "of", len(dataDec))
			print ("tree depth", rt.get_tree_depth())

		label = li[0]
		easting = int(li[2])	
		northing = int(li[3])

		entry = rt.insert((easting, northing), label)
		entries.append(entry)		

		if searchBox.contains((easting, northing)):
			bruteForceSearch.append(((easting, northing), label))

		#PrintTree(rt.rn)
		if rt.debug:
			rt.check_integrity()

	rt.check_integrity()

	print ("---------------")

	#PrintTree(rt.rn)

	print ("entries in tree", len(rt))
	print ("tree depth", rt.get_tree_depth())

	searchMetrics = {}
	result = rt.search(searchBox.shape, None, searchMetrics)
	print ("search results", len(result))
	print ("brute force search", len(bruteForceSearch))
	print ("leaves searched", len(searchMetrics['leaf_searched']))
	if 'leaf_skipped' in searchMetrics:
		print ("leaves skipped", len(searchMetrics['leaf_skipped']))

	print ("Delete some nodes")
	random.shuffle(entries)
	for i in range(100):
		entryToDelete = entries.pop()
		rt.delete(entryToDelete)
		if rt.debug:
			rt.check_integrity()

	print ("entries in tree", len(rt))

	print ("And reinsert more stuff")
	dataDec = dataDecOriginal[1000:2000]
	for i, li in enumerate(dataDec):

		label = li[0]
		easting = int(li[2])	
		northing = int(li[3])

		entry = rt.insert((easting, northing), label)
		if rt.debug:
			rt.check_integrity()

	rt.check_integrity()
	print ("entries in tree", len(rt))

	if True:
		fig,ax = plt.subplots(1)
		PlotTree(rt.rn, fig, ax)
		ax.set_xlim(rt.rn.mbr.shape[0], rt.rn.mbr.shape[2])
		ax.set_ylim(rt.rn.mbr.shape[1], rt.rn.mbr.shape[3])
		plt.show()

